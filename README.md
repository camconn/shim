```
     _     _
 ___| |__ (_)_ __ ___
/ __| '_ \| | '_ ` _ \
\__ \ | | | | | | | | |
|___/_| |_|_|_| |_| |_|

SHIM Hugo Interface & Manager
```

**SHIM is beta™ software. Please use backups!**

SHIM is an interface for [Hugo](https://gohugo.io). It provides a friendly interface
on top of Hugo's speed and power.

# Why?
One traditional downside of static site generators is that the writer cannot see
how their content will look until after they run a generation command.

SHIM solves this by handling all interaction with the with Hugo, including
changing site settings, saving posts, and generating preview and public builds
automatically for the user. This allows content creators to focus on what they
do best: producing content.

# Setup
It takes a few steps to set up SHIM. Before attempting this guide, make sure
that Go 1.5 or later is installed, and that you have a working `$GOBIN`,
`$GOPATH`, and `$GOROOT`.

## Dependencies
First, [install Hugo](https://github.com/gohugoio/hugo#choose-how-to-install).
After that, SHIM's dependencies must be satisfied. To do this, use
[manul](https://github.com/kovetskiy/manul).

## Building and Using SHIM
Now it's time to install SHIM. Run the following commands in a terminal. This will
fetch all of the requisite dependencies, and run shim.


```
$ go get gitlab.com/camconn/SHIM
$ cd $GOPATH/src/gitlab.com/camconn/SHIM
$ manul -I
$ go build
$ ./shim

# The default credentials for your SHIM instance will be root:hunter2
# You can access SHIM at `127.0.0.1:8080`, unless you modified the configuration
# to point somewhere else.
#
# Please go to the "Settings" page and change your password.
```

For running on Windows, you may need to exchange `$GOPATH` with `%GOPATH%`.

After all this, SHIM will have created a test site, `test` for you in the `sites/` folder.

By default, SHIM opens an http server at 127.0.0.1 on port 8080, however it will
use a different port if the `PORT` environment variable is set.

To run SHIM on top of a dynamic reload utility like [gin](https://github.com/codegangsta/gin),
which is useful for developing SHIM, you can run the following command:
```
$ gin -p 8080 run
```

SHIM-specific settings may be changed by modifying `config.toml`.

## Set-up your Public Site
Currently, SHIM does not serve your public website to visitors, so it is more
convenient to use a dedicated web server such as *Nginx* or *Apache* to host your
content. An example *NGINX* configuration file is located in `nginx-example.conf`.

# Contributing
To contribute to SHIM, please file a merge request on [Gitlab](https://gitlab.com/camconn/shim), or
just [send me an email](https://www.camconn.cc/about) with a patch.

For now, there is no code of conduct for contributing to SHIM. This may change
in the future. For now, just use common sense and act mature when interacting
with others.

# Licensing
SHIM is released under the GNU Affero General Public License, Version 3 or any
later version. A copy of this license can be found in the file `LICENSE`.

The source code for this program is available on [Gitlab](https://gitlab.com/camconn/SHIM).

SHIM uses some third party libraries to enhance your experience. Specifically,
SHIM uses [Bulma](http://bulma.io),
[Awesomplete](https://github.com/LeaVerou/awesomplete), and
[SimpleMDE](https://github.com/NextStepWebs/simplemde-markdown-editor) which are
all licensed under the MIT License.

A copy of each license for each third party resource may be found in
`3RD-PARTY-LICENSES`.
